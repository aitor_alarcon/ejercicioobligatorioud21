package Views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.SystemColor;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Principal extends JFrame {

	private JPanel contentPane;
	
	JTextArea textArea_1;
	
	JTextArea textArea;
	
	JTextArea Operacion;
	
	JTextArea historial;
	
	private String operador;
	
	private double num2;
	
	private double num1;
	
	private double resultado;

	public Principal() {
		setTitle("Calculadora");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 771, 677);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("%");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				operador = "%";
				num2 =  Double.parseDouble(textArea_1.getText());
				Operacion.setText(textArea_1.getText() + "+");
				textArea_1.setText(" ");
			}
		});
		btnNewButton.setBounds(0, 181, 105, 64);
		contentPane.add(btnNewButton);
		
		JButton btnCe = new JButton("CE");
		btnCe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea_1.setText(null); 
				Operacion.setText(null);
			}
		});
		btnCe.setBounds(117, 181, 105, 64);
		contentPane.add(btnCe);
		
		JButton btnC = new JButton("C");
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea_1.setText(null);
			}
		});
		btnC.setBounds(234, 181, 105, 64);
		contentPane.add(btnC);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea_1.setText(""+textArea_1.getText().substring(0, textArea_1.getText().length() - 1));
			}
		});
		btnBorrar.setBounds(351, 181, 105, 64);
		contentPane.add(btnBorrar);
		
		JButton btnx = new JButton("1/X");
		btnx.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num2 =  1 / Double.parseDouble(textArea_1.getText());
				Operacion.setText(String.valueOf(num2));
				textArea_1.setText(String.valueOf(num2));
			}
		});
		btnx.setBounds(0, 258, 105, 64);
		contentPane.add(btnx);
		
		JButton btnX = new JButton("X^2");
		btnX.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num2 =  Double.parseDouble(textArea_1.getText()) * Double.parseDouble(textArea_1.getText());
				Operacion.setText(String.valueOf(num2));
				textArea_1.setText(String.valueOf(num2));
			}
		});
		btnX.setBounds(117, 258, 105, 64);
		contentPane.add(btnX);
		
		JButton btnx_1 = new JButton("2√x");
		btnx_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num2 =  Math.sqrt(Double.parseDouble(textArea_1.getText()));
				Operacion.setText(String.valueOf(num2));
				textArea_1.setText(String.valueOf(num2));
			}
		});
		btnx_1.setBounds(234, 258, 105, 64);
		contentPane.add(btnx_1);
		
		JButton btnNewButton_7 = new JButton("/");
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				operador = "/";
				num2 =  Double.parseDouble(textArea_1.getText());
				Operacion.setText(textArea_1.getText() + "/");
				textArea_1.setText(" ");
			}
		});
		btnNewButton_7.setBounds(351, 258, 105, 64);
		contentPane.add(btnNewButton_7);
		
		JButton btnNewButton_8 = new JButton("7");
		btnNewButton_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea_1.setText(getTexto() + "7"); 
			}
		});
		btnNewButton_8.setBounds(0, 335, 105, 64);
		contentPane.add(btnNewButton_8);
		
		JButton btnNewButton_1_1 = new JButton("8");
		btnNewButton_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea_1.setText(getTexto() + "8"); 
			}
		});
		btnNewButton_1_1.setBounds(117, 335, 105, 64);
		contentPane.add(btnNewButton_1_1);
		
		JButton btnNewButton_2_1 = new JButton("9");
		btnNewButton_2_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea_1.setText(getTexto() + "9"); 
			}
		});
		btnNewButton_2_1.setBounds(234, 335, 105, 64);
		contentPane.add(btnNewButton_2_1);
		
		JButton btnNewButton_3_1 = new JButton("X");
		btnNewButton_3_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				operador = "*";
				num2 =  Double.parseDouble(textArea_1.getText());
				Operacion.setText(textArea_1.getText() + "X");
				textArea_1.setText(" ");
			}
		});
		btnNewButton_3_1.setBounds(351, 335, 105, 64);
		contentPane.add(btnNewButton_3_1);
		
		JButton btnNewButton_4_1 = new JButton("4");
		btnNewButton_4_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea_1.setText(getTexto() + "4"); 
			}
		});
		btnNewButton_4_1.setBounds(0, 412, 105, 64);
		contentPane.add(btnNewButton_4_1);
		
		JButton btnNewButton_5_1 = new JButton("5");
		btnNewButton_5_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea_1.setText(getTexto() + "5"); 
			}
		});
		btnNewButton_5_1.setBounds(117, 412, 105, 64);
		contentPane.add(btnNewButton_5_1);
		
		JButton btnNewButton_6_1 = new JButton("6");
		btnNewButton_6_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea_1.setText(getTexto() + "6"); 
			}
		});
		btnNewButton_6_1.setBounds(234, 412, 105, 64);
		contentPane.add(btnNewButton_6_1);
		
		JButton btnNewButton_7_1 = new JButton("-");
		btnNewButton_7_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				operador = "-";
				num2 =  Double.parseDouble(textArea_1.getText());
				Operacion.setText(textArea_1.getText() + "-");
				textArea_1.setText(" ");
			}
		});
		btnNewButton_7_1.setBounds(351, 412, 105, 64);
		contentPane.add(btnNewButton_7_1);
		
		JButton btnNewButton_9 = new JButton("1");
		btnNewButton_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea_1.setText(getTexto() + "1"); 
			}
		});
		btnNewButton_9.setBounds(0, 489, 105, 64);
		contentPane.add(btnNewButton_9);
		
		JButton btnNewButton_1_2 = new JButton("2");
		btnNewButton_1_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea_1.setText(getTexto() + "2"); 
			}
		});
		btnNewButton_1_2.setBounds(117, 489, 105, 64);
		contentPane.add(btnNewButton_1_2);
		
		JButton btnNewButton_2_2 = new JButton("3");
		btnNewButton_2_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea_1.setText(getTexto() + "3"); 
			}
		});
		btnNewButton_2_2.setBounds(234, 489, 105, 64);
		contentPane.add(btnNewButton_2_2);
		
		JButton btnNewButton_3_2 = new JButton("+");
		btnNewButton_3_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				operador = "+";
				num2 =  Double.parseDouble(textArea_1.getText());
				Operacion.setText(textArea_1.getText() + "+");
				textArea_1.setText(" ");
			}
		});
		btnNewButton_3_2.setBounds(351, 489, 105, 64);
		contentPane.add(btnNewButton_3_2);
		
		JButton btnNewButton_5_2 = new JButton("0");
		btnNewButton_5_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea_1.setText(getTexto() + "0"); 
			}
		});
		btnNewButton_5_2.setBounds(0, 566, 222, 64);
		contentPane.add(btnNewButton_5_2);
		
		JButton btnNewButton_6_2 = new JButton(",");
		btnNewButton_6_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea_1.setText(getTexto() + "."); 
			}
		});
		btnNewButton_6_2.setBounds(234, 566, 105, 64);
		contentPane.add(btnNewButton_6_2);
		
		JButton btnNewButton_7_2 = new JButton("=");
		btnNewButton_7_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num1 = Double.parseDouble(textArea_1.getText());
				if (operador.contentEquals("+")) {
					resultado = num2 + num1;
					textArea_1.setText(String.valueOf(resultado));
					Operacion.setText(String.valueOf(resultado));
					historial.setText(historial.getText() + (num2 + "+" + num1 + "=" + resultado + "\n"));
				}else if (operador.contentEquals("-")) {
					resultado = num2 - num1;
					textArea_1.setText(String.valueOf(resultado));
					Operacion.setText(String.valueOf(resultado));
					historial.setText(historial.getText() + (num2 + "-" + num1 + "=" + resultado + "\n"));
				}else if (operador.contentEquals("/")) {
					resultado = num2 / num1;
					textArea_1.setText(String.valueOf(resultado));
					Operacion.setText(String.valueOf(resultado));
					historial.setText(historial.getText() + (num2 + "/" + num1 + "=" + resultado + "\n"));
				}else if (operador.contentEquals("*")) {
					resultado = num2 * num1;
					textArea_1.setText(String.valueOf(resultado));
					Operacion.setText(String.valueOf(resultado));
					historial.setText(historial.getText() + (num2 + "*" + num1 + "=" + resultado + "\n"));
				}else if (operador.contentEquals("%")) {
					resultado = (num1/100) * num2;
					textArea_1.setText(String.valueOf(resultado));
					Operacion.setText(String.valueOf(resultado));
					historial.setText(historial.getText() + (num2 + "%" + num1 + "=" + resultado + "\n"));
				}
				
			}
		});
		btnNewButton_7_2.setBackground(SystemColor.activeCaption);
		btnNewButton_7_2.setBounds(351, 566, 105, 64);
		contentPane.add(btnNewButton_7_2);
		
		JLabel lblNewLabel = new JLabel("Historial");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel.setBounds(490, 32, 128, 43);
		contentPane.add(lblNewLabel);
		
		 historial = new JTextArea();
		historial.setBounds(490, 113, 251, 504);
		contentPane.add(historial);
		
		 textArea_1 = new JTextArea();
		textArea_1.setBounds(12, 113, 444, 43);
		contentPane.add(textArea_1);
		
		 Operacion = new JTextArea();
		Operacion.setBounds(12, 45, 444, 43);
		contentPane.add(Operacion);
		
	}
	
	public String getTexto() {
		String texto = "";
		texto = textArea_1.getText();
		return texto;
	}
}
